from pymongo import MongoClient
from backend import config


# Authentication mechanisms
SCRAM_SHA_1 = "SCRAM-SHA-1"
MONGODB_CR = "MONGODB-CR"


class MongoDb(object):
    def __init__(self, host, database, port=27017, user=None, password=None, mechanism=SCRAM_SHA_1):
        connection = MongoClient(host=host, port=port)
        self.db = connection[database]
        if user and password:
            self.db.authenticate(user, password, mechanism=mechanism)

mongo_connection = MongoDb(config.mongo_server, config.database, config.port, config.user, config.password).db
