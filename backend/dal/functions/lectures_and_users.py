from backend.dal.mongo_client.mongodb_client import mongo_connection



def add_user(user_data_json):
    users_collection = mongo_connection.users.insert_one(user_data_json)

def add_lecture_to_user():
    pass

def add_review_for_user():
    pass

def add_tag_for_user():
    pass

def remove_tag_for_user():
    pass

def get_tags_for_user():
    pass

def get_user_data(username):
    pass

def edit_user_data():
    pass

def get_user_contact_detail(username):
    user_data = get_user_data(username)
    return user_data["contact_details"]

def add_review_for_lecture():
    pass

def edit_lecture():
    pass

def create_new_lecture():
    pass

def add_tag_for_lecture():
    pass

def remove_tag_for_lecture():
    pass

def get_lecture_tags():
    pass
