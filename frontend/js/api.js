let api = function(){
    let get = function(url){
        return new Promise((accept, reject) => {
            fetch(url, {
                method: 'GET'
            }).then(function (response) {
                response.json().then(res => {
                    accept(res);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
    };

    let post = function(url, payload){
        return new Promise((accept, reject) => {
            fetch(url, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    'Accept': 'application/json, text/plain, */*'
                },
                body: JSON.stringify(payload)
            }).then(function (response) {
                if (response.ok){
                    response.json().then(res => {
                        accept(res);
                    });
                }
                else{
                    reject(response.statusText)
                }
            }).catch(function (err) {
                reject(err);
            });
        });
    };

    return {
        search: function(text){
            return post(URLS.SEARCH, {
                text: text
            });
        }
    }
}();


let URLS = {
    SEARCH: "/search"
};
