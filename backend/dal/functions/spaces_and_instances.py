from backend.dal.mongo_client.mongodb_client import mongo_connection


def get_all_spaces():
    spaces_collection = mongo_connection.spaces.find({})

    documents = []
    for document in spaces_collection:
        document["_id"] = str(document["_id"])
        documents.append(document)
    return documents


def get_my_spaces():
    pass

def add_user_to_space():
    pass

def remove_user_from_space():
    pass

def add_instance_to_space():
    pass

def get_instances_for_space():
    pass

def add_review_for_instance():
    pass

def edit_instance():
    pass

def add_tag_for_instance():
    pass

def remove_tag_for_instance():
    pass

def get_tags_for_instance():
    pass










