import inspect
import json
import sys
import json

import os
from backend.adapters import request_adapter
from backend.adapters.request_adapter import request_adapter_wrapper
from backend.adapters.response_adapter import response_adapter_wrapper
from backend.config import frontend_path
<<<<<<< HEAD
from backend.validators import input_validator
from flask import Flask, send_file, request, Response
from backend.logs.logger import config, logger
from backend.dal.functions.spaces_and_instances import get_all_spaces
from backend.dal.functions.lectures_and_users import *
=======
from backend.dal.functions.spaces_and_instances import get_all_spaces
from backend.entities.base_request import BasRequest
from backend.logs.logger import config, logger
from flask import Flask, send_file
from flask import Response
>>>>>>> 36706a05e420a418b30bbaf1cf201b585343292a

sys.path.append(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))

# initialize Flask
app = Flask(__name__, static_folder="../")
logger.info(" ################## SEARCH STARTING ##################" +
            "\n - Configuration name: " + config.name)


@app.route("/")
def index():
    return send_file(os.path.join(frontend_path, "index.html"))


@app.route("/<path:path>")
def send_static_file(path):
    return app.send_static_file(path)


@app.route('/get_spaces', methods=['POST'])
@response_adapter_wrapper("application/json")
@request_adapter_wrapper(BasRequest)
def get_spaces(request):
    all_sapces = get_all_spaces()
    return {"spaces": all_sapces}


@app.route('/search', methods=['POST'])
def search(text, text2):
    return "nothing"


# @app.route('/get_user_tags', methods=['POST'])
# def get_user_tags():
#     user_tags = get_tags_for_user()
#     user_tags = json.dumps({"user_tags": user_tags})
#     return Response(user_tags, 200, mimetype="application/json")


# @app.route('/get_lecture_tags', methods=['POST'])
# def get_user_tags():
#     lecture_tags = get_tags_for_lecture()
#     lecture_tags = json.dumps({"lecture_tags": lecture_tags})
#     return Response(lecture_tags, 200, mimetype="application/json")


# @app.route('/get_instance_tags', methods=['POST'])
# def get_user_tags():
#     instance_tags = get_tags_for_instance()
#     instance_tags = json.dumps({"instance_tags": instance_tags})
#     return Response(instance_tags, 200, mimetype="application/json")


@app.route('/insert_user', methods=['POST'])
def insert_user(request):
    data = json.loads(request.data)
    data_json = {"contact_details": data["contact_details"], "avaibility": data["avaibility"], "tags": data["tags"]}
    add_user(data_json)
    return Response(data_json, 200, mimetype="application/json")