customElements.define("x-dyslexify", class extends HTMLElement {
    constructor() {
        super();
        let shadowRoot = this.attachShadow({mode: 'open'});
        shadowRoot.innerHTML = `
            <style>
            :host {
            
            }
            </style>
            
            <x-header></x-header>
            <x-editor></x-editor>
            <x-footer></x-footer>
        `;
    }
});
